import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Catalog from '../pages/Catalog/Catalog'
import Home from '../pages/Home/Home'

export default function Router() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/catalogo/:path" component={Catalog} />
            </Switch>
        </BrowserRouter>
    )
}