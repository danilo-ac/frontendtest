export const goToHome = (history) => {
    history.push('/')
}

export const goToContact = (history) => {
    history.push('/contato')
}

export const goToCatalog = (history, path) => {
    history.push(`/catalogo/${path}`)
}
