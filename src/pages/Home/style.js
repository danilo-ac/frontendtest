import styled from "styled-components";
import * as COLORS from '../../constants/colors'

export const Container = styled.body`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: fit-content;
`

export const Main = styled.main`
    display: flex;
    justify-content: stretch;
    align-items: flex-start;
    width: 1024px;
    height: fit-content;
    margin: 2% auto 2% auto;
    section{
        width: 100%;
        height: fit-content;
        margin-left: 2%;
        article{
            height: 100%;
            p{
                text-align: justify;
                height: fit-content;
            }
        }
    }
`
export const ImageHome = styled.img`
    width: 100%;
    height: 250px;
    background-color: ${COLORS.GRAY_MIDDLE};
`