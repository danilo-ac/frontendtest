import React from 'react'
import Header from '../../components/Header/Header'
import NavbarSide from '../../components/NavbarSide/NavbarSide'
import Footer from '../../components/Footer/Footer'
import { Container, Main, ImageHome } from './style'
import * as text from './text.json'


export default function Home() {
    return (
        <Container>
            <Header />
            <Main>
                <NavbarSide />
                <section>
                    <ImageHome />
                    <article>
                        <h1>{text['pt-br'].title}</h1>
                        <p>{text['pt-br'].text}</p>
                    </article>
                </section>
            </Main>
            <Footer />
        </Container>
    )
}