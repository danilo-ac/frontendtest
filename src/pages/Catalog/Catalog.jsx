import { useParams } from "react-router"
import { Container, Main } from "./style"
import Header from '../../components/Header/Header'
import Footer from "../../components/Footer/Footer"
import Breadcrumbs from "../../components/Breadcrumbs/Breadcrumbs"
import FilterSide from "../../components/FilterSide/FilterSide"
import ProductList from "../../components/ProductList/ProducList"


export default function Catalog() {

    const { path } = useParams()

    return <Container>
        <Header />
        <Breadcrumbs path={path} />
        <Main>
            <FilterSide />
            <ProductList/>
        </Main>
        <Footer />
    </Container>
}