import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: fit-content;
`
export const Main = styled.main`
    display: flex;
    width: 1024px;
    height: fit-content;
    margin: 0 auto 2%  auto;
`