import React, { useState, useEffect } from "react"
import { GlobalStateContext } from "./GlobalStateContext"
import useRequestData from "../hooks/useRequestData"
import setPages from "../services/setPages"
import axios from 'axios'
import { BASE_URL } from '../constants/urls'


const GlobalState = (props) => {

    const data = useRequestData("", "list")
    const allPages = data && setPages(data)
    const [path, setPath] = useState("")
    const [pageId, setPageId] = useState("")
    const [pageName, setPageName] = useState("")
    const [products, setProducts] = useState({})

    useEffect(() => {
        if (path) {
            for (let item of allPages) {
                if (item.path === path) {
                    setPageId(item.id)
                    setPageName(item.name)
                    break
                }
            }
        }
    }, [path])

    const states = { allPages, pageName, products }
    const setters = { setPath, setProducts }
    // const requests = {}
    // // const functions = {}

    return (
        <GlobalStateContext.Provider value={{ states, setters }}>
            {props.children}
        </GlobalStateContext.Provider>
    )
}

export default GlobalState