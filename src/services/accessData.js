import axios from "axios"

const accesstData = (path) => {
    const config = {
        method: 'get',
        url: path
    }
    axios(config)
        .then((res) => {
            return res.data
        })
        .catch((err) => {
            window.alert('Erro ao realizar solicitação.\n Tente novamente.')
        })

}