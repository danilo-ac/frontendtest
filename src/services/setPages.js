export default function setPages(data) {
    const pages = [
        {
            id: 0,
            name: "Página Inicial",
            path: ""
        },
        {
            id: 999999,
            name: "Contato",
            path: "contato"
        }
    ]

    pages.splice(1, 0, data.items)

    const newPages = pages.flat(Infinity)

    return newPages
}