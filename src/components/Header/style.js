import styled from "styled-components";
import logotypeImage from "../../media/logo.jpg"

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: fit-content;
`

export const ContainerLogoAndSearch = styled.div`
    display: flex;
    align-items: center;
    justify-content:space-between;
    width: 1024px;
    height: 112px;
`
export const Logotype = styled.img`
    content: url(${logotypeImage});
    src: url(${logotypeImage});
    cursor: pointer;
`