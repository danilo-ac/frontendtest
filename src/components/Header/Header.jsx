import { useHistory } from "react-router";
import { goToHome } from "../../routes/coordinator";
import NavbarTop from "../NavbarTop/NavbarTop";
import Search from "../Search/Search";
import SignInLogIn from "../SignInLogIn/SignInLogIn";
import { Container, ContainerLogoAndSearch, Logotype } from "./style";

export default function Header() {
    const history = useHistory()
    return <Container>
        <SignInLogIn />
        <ContainerLogoAndSearch>
            <Logotype alt={"Webjump Logotipo"} onClick={()=>goToHome(history)}/>
            <Search />
        </ContainerLogoAndSearch>
        <NavbarTop />
    </Container>
}