import { useContext } from "react"
import { GlobalStateContext } from "../../global/GlobalStateContext"
import ProductCard from "../ProductCard/ProductCard"
import { Container } from './style'

export default function ProductList() {

    const { states } = useContext(GlobalStateContext)

    const { products } = states

    const displayProducts = !products ? [] : Array.from(products).map(item => {
        return <ProductCard key={item.id} item={item} />
    })

    return (
        <Container>{displayProducts}</Container>
    )
}