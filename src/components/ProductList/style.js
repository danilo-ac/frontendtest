import styled from "styled-components";

export const Container = styled.div`
    display: grid;
    grid-template-columns: repeat(4,1fr);
    justify-items: center;
    row-gap: 4%;
    width: 100%;
    margin-bottom: 10%;
`