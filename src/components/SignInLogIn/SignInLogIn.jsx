import { useContext } from "react"
import { useParams } from "react-router"
import { GlobalStateContext } from "../../global/GlobalStateContext"
import { Container, FlexContainer } from "./style"

export default function SignInLogIn() {

    const { path } = useParams()

    const { setters } = useContext(GlobalStateContext)
    
    const { setPath } = setters

    setPath && path && setPath(path)

    return <Container>
        <FlexContainer>
            <p><span>Acesse sua Conta</span> ou <span>Cadastre-se</span></p>
        </FlexContainer>
    </Container>

}