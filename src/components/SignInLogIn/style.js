import styled from 'styled-components'
import * as COLORS from '../../constants/colors'

export const Container = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    background-color: ${COLORS.GRAY_DARK};
`
export const FlexContainer = styled.div`
    width: 1024px;
    display: flex;
    justify-content: right;
    background-color: ${COLORS.GRAY_DARK};
        p{
            font-size: small;
            color: white;
                span{                    
                    font-weight:800;
                    text-decoration: underline;
                    cursor: pointer;
                    :active{
                        opacity: 0.8;
                    }
                    :visited{
                        text-decoration: none;
                    }
                }
        }
`