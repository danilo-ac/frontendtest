import { useHistory } from "react-router"
import { goToCatalog, goToContact, goToHome } from "../../routes/coordinator"
import { Container, FlexContainer } from "./style"
import useRequesData from '../../hooks/useRequestData'
import setPages from "../../services/setPages"

export default function NavbarTop() {
    
    const data = useRequesData("", 'list')
    
    const allPages = setPages(data)
    
    const history = useHistory()

    const displayCategories = !data ? false : allPages
        .map(item => <li
            key={item.id}
            onClick={() => !item.path ?
                goToHome(history)
                : item.id === 999999 ?
                    goToContact(history)
                    : goToCatalog(history, item.path)}
        > {item.name}</li>)

    return <Container>
        <FlexContainer>
            {displayCategories}
        </FlexContainer>
    </Container>

}