import styled from 'styled-components'
import * as COLORS from '../../constants/colors'

export const Container = styled.div`
    width: 100%;
    height: 48px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${COLORS.RED};
`
export const FlexContainer = styled.div`
    display: flex;
    justify-content: left;
    width: 1024px;
    padding-left: 2%;
    background-color: ${COLORS.RED};
    li{
        margin-right: 5%;
        font-family: 'Open Sans';
        font-weight:800;
        font-size: medium;
        text-decoration: none;
        list-style:none;
        color: white;
        text-transform: uppercase;
        cursor: pointer;
        :active{
            opacity: 0.8;
        }
        :visited{
            text-decoration: none;
        }
    }
        
`