import styled from 'styled-components'
import * as COLORS from '../../constants/colors'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { red } from '@material-ui/core/colors';

export const Container = styled.div`
    height: 42px;
    display: flex;
    justify-content: center;
    .search-box{
        width: 20vmax;
        margin: 0%;
    }
`

export const ColorButton = withStyles((theme) => ({
    root: {
        padding: '6px 24px',
        color: COLORS.WHITE,
        backgroundColor: COLORS.RED,
        '&:hover': {
            backgroundColor: red[900],
          },
    },
}))(Button);