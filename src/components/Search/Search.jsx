import { OutlinedInput } from "@material-ui/core"
import { Container , ColorButton } from "./style"
import useForm from "../../hooks/useForm"


export default function Search() {

    const { input, onChangeInput } = useForm({ search: "" })

    return <Container>
        <OutlinedInput
            className={"search-box"}
            type="text"
            onChange={onChangeInput}
            value={input.search}
            name={"search"}
            inputProps={{ 'aria-label': 'search' }}
            placeholder={""}
        />
        <ColorButton
        >Buscar</ColorButton>
    </Container>
}