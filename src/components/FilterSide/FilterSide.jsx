import React, { useContext } from "react";
import { useHistory, useParams } from "react-router";
import { GlobalStateContext } from "../../global/GlobalStateContext";
import useRequestData from "../../hooks/useRequestData";
import { goToCatalog } from "../../routes/coordinator";
import { Container, ColorPaletteContainer, ColorPalette } from "./style";

export default function FilterSide() {

    const allCategories = useRequestData("", "list")

    const { path } = useParams()

    const productDB = !allCategories ? [] : allCategories.items.find(item => {
        return item.path === path
    })?.id

    const { filters, items } = useRequestData("", productDB)

    const { setters } = useContext(GlobalStateContext)

    const { setProducts } = setters

    items && setProducts(items)

    const history = useHistory()

    const displayCategories = !allCategories ? false : allCategories.items.map(item => {
        return <li key={item.id} onClick={() => goToCatalog(history, item.path)}>
            {item.name}
        </li>
    })

    /*
    Elaborei o código abaixo para obter valores únicos dos filtros.
    Primeiramente busca as keys já definidas na propriedade 'filters' e seu valor, verificando no formato:
    {"filters": [ {"gender": "Gênero"},{"filter1": "filterName1"}, {filter2: "FilterName2"},... ]
    Depois busca na propriedade 'items' os valores de cada filtro do produto, e caso um diferente não contido na
    propriedade 'filters', criará uma novo e dará o nome da sua própria chave, verificando no formato:
    {"items": [{...,"filter": [{"gender": "Masculina"},{"umNovoFiltroNãoPrevisto": "valorDesseNovoFiltro"}]}
    Posteriormente fará uma operação para obter valores únicos para exibição final

    */
    let availableFilters = []

    if (filters && items) {
        let filterValues = {}
        filters.forEach((item) => {
            let name = Object.values(item).toString();
            filterValues[Object.keys(item)] = {
                name: name,
                values: []
            };
        });

        items.forEach((item) => {
            item.filter.forEach((item) => {
                let keyName = Object.keys(item).toString();

                if (!filterValues[keyName]) {
                    filterValues[keyName] = {
                        name: keyName,
                        values: []
                    };
                }

                filterValues[keyName].values = [
                    ...filterValues[keyName].values,
                    item[keyName]
                ]
            })
        })

        for (let item in filterValues) {
            const uniqueValues = filterValues[item].values
                .sort((a, b) => {
                    return a.localeCompare(b)
                })
                .filter((item, index, array) => {
                    if (index === 0) {
                        return item
                    } else if (index === array.length - 1) {
                        if (item !== array[index - 1]) {
                            return item
                        }
                    } else {
                        if (item !== array[index - 1]) {
                            return item
                        }
                    }
                })

            filterValues[item].values = uniqueValues
        }

        for (let [key, value] of Object.entries(filterValues)) {
            availableFilters.push(value)
        }
    }

    const displayProductFilters = !availableFilters ? [] : availableFilters.map(item => {
        return <React.Fragment>
            <h3>{item.name.toUpperCase()}</h3>
            {
                item.name.toLowerCase().trim() !== "cor" ?
                    item.values.map(value => { return <li key={value}>{value}</li> })
                    : <ColorPaletteContainer>{
                        item.values.map(value => {
                            return <ColorPalette alt={value.toLowerCase().trim()}
                                key={value} title={value} />
                        })
                    }</ColorPaletteContainer>
            }
        </React.Fragment>
    })

    return <Container>
        <h1>FILTRE POR</h1>
        <h3>CATEGORIAS</h3>
        {displayCategories}
        {displayProductFilters}
    </Container>
}