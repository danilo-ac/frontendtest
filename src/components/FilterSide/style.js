import styled from "styled-components";
import * as COLORS from '../../constants/colors'

export const Container = styled.nav`
    display: flex;
    flex-direction: column;
    width: 28%;
    height: 100%;
    padding: 1%;
    border: 1px solid ${COLORS.GRAY_LIGHT};
    h1{
        margin: 0;
        padding: 0;
        color: ${COLORS.RED}
    };
    h3{
        margin-bottom: 2%;
        padding: 0;
        color: ${COLORS.BLUE_MIDDLE}
    }
    li{
        margin: 0;
        padding: 0;
        cursor: pointer;
        color: ${COLORS.GRAY_MIDDLE};
        line-height: 1.8em;
        
    }
`

export const ColorPaletteContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
`

export const ColorPalette = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 48px;
    height:28px;
    margin: 0 2% 2% 0;
    background-color: ${(props) => {
        switch (props.alt) {
            case 'amarela':return `yellow`
            case 'azul': return `blue`
            case 'bege': return `#BF9B7A`
            case 'cinza': return `gray`
            case 'laranja': return `orange`
            case 'preto' : return `black`
            case 'preta' : return `black`
            case 'rosa' : return `pink`
            default: return `white;
            border: 1px solid lightgray;
            :before{
                content: "${props.alt}";
                font-size: xx-small
            }`
        }
    }}
`