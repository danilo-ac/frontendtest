import styled from 'styled-components'
import * as COLORS from '../../constants/colors'

export const Container = styled.footer`
    width: 1024px;
    height: 200px;
    margin: 0 auto 2% auto;
    background-color: ${COLORS.RED};    
`