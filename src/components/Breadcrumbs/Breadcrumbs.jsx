import { useContext } from "react"
import { useHistory, useParams } from "react-router"
import { GlobalStateContext } from "../../global/GlobalStateContext"
import { goToHome } from "../../routes/coordinator"
import { Container } from "./style"

export default function Breadcrumbs(props) {

    const {path} = useParams()
    const {states} = useContext(GlobalStateContext)
    const {pageName} = states

    const history = useHistory()

    return <Container>
        <p>
            <span id={"home-path"} onClick={() => goToHome(history)}>
                Página Inicial
            </span>
            <span id={"element-path"} >{" > "}</span>
            <span id={"category-path"}>{pageName || path}</span>
        </p>


    </Container>
}