import styled from "styled-components";
import * as COLORS from '../../constants/colors'

export const Container = styled.div`
    display: flex;
    align-items: center;
    width: 1024px;
    margin: 0 auto;
    p{
        font-size: small;
        #home-path{
            cursor: pointer;
        }
        #element-path{
            color: ${COLORS.GRAY_MIDDLE}
        };
        #category-path{
            color: ${COLORS.RED}
        }
    }
`