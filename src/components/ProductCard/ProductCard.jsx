import { Container, BuyButton } from "./style";

export default function ProductCard(props) {
    const {
        id,
        sku,
        path,
        name,
        image,
        price,
        specialPrice
    } = props.item


    const formatedPrice = new Intl.NumberFormat('pt-BR', {style: 'currency' , currency: "BRL", maximumFractionDigits: 2 }).format(price)

    const formatedSpecialPrice = new Intl.NumberFormat('pt-BR', {style: 'currency' , currency: "BRL", maximumFractionDigits: 2 }).format(specialPrice)

    return <Container>
        <img alt={name} title={name} src={process.env.PUBLIC_URL + `/${image.toString().trim()}`} />
        <p id={"name"}>{name}</p>
        {specialPrice ?
            <div id={"price-container"}>
                <p id={"old-price"}>{formatedPrice}</p>
                <p id={"new-price"}>{formatedSpecialPrice}</p>
            </div>
            : <div id={"price-container"}>
                <p id={"price"}>{formatedPrice}</p>
            </div>
        }
        <BuyButton
            fullWidth
        >Comprar</BuyButton>
    </Container>
}