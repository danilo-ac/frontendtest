import styled from "styled-components";
import * as COLORS from '../../constants/colors'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const size = '158px'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: ${size};
    height: 318px;
    img{
        width: ${size};
        height: ${size};
        object-fit: contain;
        -o-object-fit: contain;
        border: 1px solid ${COLORS.GRAY_LIGHT};
    }
    #name{
        margin: 8% 0;
        flex-grow: 1;
        font-size: large;
        text-align: center;
        text-transform: uppercase;
        color: ${COLORS.GRAY_MIDDLE};
    }
    #price-container{
        display: flex;
        align-items: center;
        justify-content: space-evenly;
        width: 100%;
        
        #old-price{
            margin: 0;
            padding: 0;
            font-size: medium;
            font-weight: bolder;
            text-decoration:line-through;
            color: ${COLORS.GRAY_MIDDLE};
        }
        #price, #new-price{
            margin: 0;
            padding: 0;
            font-size: x-large;
            font-weight: bolder;
            color: ${COLORS.RED};
        }
    }
`
export const BuyButton = withStyles((theme) => ({
    root: {
        padding: '6px 24px',
        color: COLORS.WHITE,
        backgroundColor: COLORS.BLUE_LIGHT,
        '&:hover': {
            backgroundColor: COLORS.BLUE_MIDDLE,
        },
    },
}))(Button);