import styled from "styled-components";
import * as COLORS from '../../constants/colors' 

export const Container = styled.div`
    width: 32%;
    height: 100%;
    padding-bottom: 32%;
    background-color: ${COLORS.GRAY_LIGHT};
    li{
        margin: 0 0 12% 0;
        color: ${COLORS.GRAY_DARK};
        cursor: pointer;
    }
`