import { useHistory } from "react-router"
import useRequestData from "../../hooks/useRequestData"
import { goToCatalog, goToContact, goToHome } from "../../routes/coordinator"
import setPages from "../../services/setPages"
import { Container } from "./style"

export default function NavbarSide() {
    const data = useRequestData("", "list")

    const history = useHistory()

    const allPages = setPages(data)

    const displayPages = !data ? false : allPages.map(item => {
        return <li
            key={item.id}
            onClick={() => !item.path ?
                goToHome(history)
                : item.id === 999999 ?
                    goToContact(history)
                    : goToCatalog(history, item.path)}
        >{item.name}</li>
    })

    return <Container>
        <ul>{displayPages}</ul>
    </Container>
}