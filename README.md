# Teste Front End | Webjump

## Participante: Danilo A Chagas

## Síntese
- [x] [Screen Overview](https://bitbucket.org/danilo-ac/frontendtest/raw/9055d89606019ef9fa89bbce93745a37c3fd7a87/public/overview-page.png)
- [x] Projeto em React
    - [x] HTML com JSX
    - [x] CSS com `Styled Components`
    - [x] Criação de lógica em Javascript para exibição dos produtos e filtros
        - foco na criação de lógica para automatização em caso de update de produtos, filtros e categorias
    - [x] Recursos adicionais aplicados: Estado Global (React Context), Route (React-Route-DOM)
- [x] 'Consumo' da API com `axios`

## WebDeploy
[danilo-ac-webjump-test](https://danilo-ac-webjump-test.surge.sh/)

## Rodar Localmente
via terminal na pasta do projeto, comando: `npm start`
[http://localhost:3000](http://localhost:3000)

## Notas
- Em desenvolvimento:
    - filtro de preço
    - recurso de alternar visualização entre lista e card
    - lógica para aplicação dos filtros
    - campo de busca operacional
    - página contato
    - página login/cadastro
    - responsividade

